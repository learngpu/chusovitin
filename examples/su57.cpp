#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <numbers>
#include <thread>

#include "utils/random/bsdf_sampler.hpp"
#include "utils/random/light_sampler.hpp"

#include "gltf-parser/include/gltf/utils/aabb.h"
#include "gltf-parser/include/gltf/utils/camera.h"
#include "gltf-parser/include/gltf/utils/offset_point.h"
#include "gltf-parser/include/gltf/utils/texture.h"
#include "gltf-parser/include/gltf/utils/triangle.h"

#include <ImfRgbaFile.h>

#include "utils/random/light_sampler.hpp"

int main() {
    using namespace gpu::random;
    Scene const scene(gltf::GLTF(std::ifstream(RES_DIR "su57.glb", std::ios::binary)));

    LightSampler const lightSampler(scene);
    BSDFSampler const bsdfSampler(scene);

    auto const trace = [&](Ray const cameraRay) noexcept {
        // vec3 const skyEmission = {10.0444893f, 7.80654102f, 5.74222998f};

        auto const luminance = [&](Scene::SurfacePoint const &q1, Scene::SurfacePoint const &q2,
                                   auto const &self) noexcept -> vec3 {
            [[maybe_unused]] auto const L = [&](Scene::SurfacePoint const &a,
                                                Scene::SurfacePoint const &b) noexcept -> vec3 {
                return self(a, b, self);
            };

            if (q1.intersection.instanceI == 8) {
                return 4.f * bsdfSampler.albedo(q1);
            }

            auto const lightSample = lightSampler.sample(q1, q2);
            vec3 const lightContribution =
                lightSample
                    .transform([&](Scene::SurfacePointSample const &sample) noexcept {
                        auto const &[q0, pdf] = sample;
                        f32 const pdfBSDF     = bsdfSampler.pdfW(q0, q1, q2);
                        f32 const w           = pdf / (pdf + pdfBSDF);
                        return bsdfSampler.BSDF(q0, q1, q2) * lightSampler.emission(q0, q1) * w / pdf;
                    })
                    .value_or(vec3(0.f));

            vec3 const bsdfContribution =
                bsdfSampler.sample(q1, q2)
                    .transform([&](Scene::SurfacePointSample const &sample) noexcept {
                        auto const &[q0, pdf] = sample;
                        f32 const pdfLight    = lightSampler.pdfW(q0, q1, q2);
                        f32 const w           = pdf / (pdf + pdfLight);
                        return bsdfSampler.BSDF(q0, q1, q2) * (lightSampler.emission(q0, q1) + L(q0, q1)) * w / pdf;
                    })
                    .value_or(vec3(0.f));
            return lightContribution + bsdfContribution;
        };

        std::optional<Scene::SurfacePoint> const hit = scene.closestHit(cameraRay);
        return hit
            .transform([&](Scene::SurfacePoint const &q1) noexcept {
                Scene::SurfacePoint const q2 = {cameraRay.pos, {}, cameraRay.dir, noTLASIntersection};

                return lightSampler.emission(q1, q2) + luminance(q1, q2, luminance);
            })
            .value_or(vec3(0.f));
    };

    u32 const width  = 1920u;
    u32 const height = 1080u;

    vec3 const cameraPosition{17.5035f, -4.5359f, 3.5242f};
    f32 const q0 = 0.619599f;
    vec3 const q{0.487769f, 0.387303f, 0.477676f};

    vec3 const initialDir{0.f, 0.f, -1.f};
    vec3 const dir =
        (q0 * q0 - dot(q, q)) * initialDir + 2.f * dot(q, initialDir) * q + 2.f * q0 * cross(q, initialDir);

    vec3 const at = cameraPosition + dir;

    Camera const camera = {cameraPosition, at, vec3{0.f, 0.f, 1.f},
                           Camera::scaleFrom(0.3456f, f32(width) / f32(height))};

    using Color = Imf::Rgba;

    std::vector<Color> color(width * height);
    std::atomic<u32> a(0);
    std::vector<std::thread> thr(std::thread::hardware_concurrency());
    std::mutex writeMutex;

    for (std::thread &t : thr)
        t = std::thread([&]() noexcept {
            while (true) {
                u32 const y = a.fetch_add(1);
                if (y >= height) return;
                {
                    std::unique_lock<std::mutex> const lock(writeMutex);
                    std::cerr << std::setw(5) << y << " / " << height << '\r';
                }

                for (u32 x = 0u; x < width; ++x) {
                    auto const sample = [=](u32) noexcept {
                        f32 const u = -1.f + 2.f * (generateUniformFloat() + f32(x)) / f32(width);
                        f32 const v = 1.f - 2.f * (generateUniformFloat() + f32(y)) / f32(height);
                        return trace(camera.castRay({u, v}));
                    };
                    u32 const N          = 64u;
                    auto const samples   = std::views::iota(0u, N) | std::views::transform(sample);
                    vec3 const c         = std::ranges::fold_left(samples, vec3(0.f), std::plus{}) / f32(N);
                    color[x + y * width] = {c.x, c.y, c.z, 1.f};
                }
            }
        });
    for (std::thread &t : thr) t.join();

    Imf::RgbaOutputFile file("su57.exr", width, height, Imf::WRITE_RGBA);
    file.setFrameBuffer(color.data(), 1, width);
    file.writePixels(height);
}
