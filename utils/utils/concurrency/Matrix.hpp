#pragma once

#include <cassert>
#include <vector>

#include <experimental/simd>

#include "utils/concurrency/Threadpool.hpp"

namespace gpu {

using vf32                             = std::experimental::native_simd<float>;
inline constexpr std::size_t vf32align = std::experimental::memory_alignment_v<vf32>;

std::size_t getMemoryWidth(std::size_t cols, std::size_t alignment, std::size_t size) {
    if (cols % (alignment / size) == 0) {
        return cols;
    } else {
        std::size_t res = (cols / (alignment / size) + 1) * (alignment / size);
        return res;
    }
}

template <typename T, std::size_t Alignment>
class Matrix {
private:
    std::size_t rows_{};
    std::size_t cols_{};
    std::size_t memoryWidth_{};
    alignas(Alignment) T* data_;

public:
    Matrix() = default;
    Matrix(std::size_t rows, std::size_t cols)
        : rows_{rows},
          cols_{cols},
          memoryWidth_{getMemoryWidth(cols, Alignment, sizeof(T))},
          data_{new(std::align_val_t{Alignment}) T[rows * memoryWidth_]} {
        for (std::size_t i = 0; i < rows_; ++i) {
            for (std::size_t j = 0; j < cols_; ++j) {
                data_[i * memoryWidth_ + j] = 0;
            }
        }
    }
    Matrix(std::size_t rows, std::size_t cols, const std::vector<T> data)
        : rows_{rows},
          cols_{cols},
          memoryWidth_{getMemoryWidth(cols, Alignment, sizeof(T))},
          data_{new(std::align_val_t{Alignment}) T[rows * memoryWidth_]} {
        for (std::size_t i = 0; i < rows_; ++i) {
            for (std::size_t j = 0; j < cols_; ++j) {
                data_[i * memoryWidth_ + j] = data[i * cols_ + j];
            }
        }
    }

    Matrix(const Matrix& m) : Matrix(m.rows_, m.cols_) {
        for (std::size_t i = 0; i < rows_; ++i) {
            for (std::size_t j = 0; j < cols_; ++j) {
                data_[i * memoryWidth_ + j] = m(i, j);
            }
        }
    }

    Matrix(Matrix&& m) noexcept : rows_{m.rows_}, cols_{m.cols_} {
        data_   = m.data_;
        m.data_ = nullptr;
    }

    Matrix& operator=(const Matrix& m) {
        if (&m == this) {
            return *this;
        }
        ::operator delete[](data_, std::align_val_t{Alignment});
        rows_        = m.rows_;
        cols_        = m.cols_;
        memoryWidth_ = m.memoryWidth_;
        data_        = new (std::align_val_t{Alignment}) T[rows_ * memoryWidth_];
        for (std::size_t i = 0; i < rows_; ++i) {
            for (std::size_t j = 0; j < cols_; ++j) {
                data_[i * memoryWidth_ + j] = m(i, j);
            }
        }
        return *this;
    }

    Matrix& operator=(Matrix&& m) noexcept {
        if (&m == this) {
            return *this;
        }
        ::operator delete[](data_, std::align_val_t{Alignment});
        rows_        = m.rows_;
        cols_        = m.cols_;
        memoryWidth_ = m.memoryWidth_;
        data_        = m.data_;
        m.data_      = nullptr;
        return *this;
    }

    ~Matrix() { ::operator delete[](data_, std::align_val_t{Alignment}); }

    [[nodiscard]] std::size_t rows() const noexcept { return rows_; }
    [[nodiscard]] std::size_t cols() const noexcept { return cols_; }

    [[nodiscard]] const T& operator()(std::size_t i, std::size_t j) const noexcept {
        return data_[i * memoryWidth_ + j];
    }
    [[nodiscard]] T& operator()(std::size_t i, std::size_t j) noexcept { return data_[i * memoryWidth_ + j]; }

    [[nodiscard]] const T* data() const noexcept { return data_; }
    [[nodiscard]] T* data() noexcept { return data_; }

    [[nodiscard]] const T* data(std::size_t i, std::size_t j) const noexcept { return data_ + i * memoryWidth_ + j; }
    [[nodiscard]] T* data(std::size_t i, std::size_t j) noexcept { return data_ + i * memoryWidth_ + j; }
};

template <typename T, std::size_t Alignment>
Matrix<T, Alignment> multiplyBasic(const Matrix<T, Alignment>& l, const Matrix<T, Alignment>& r) {
    assert(l.cols() == r.rows());
    Matrix<T, Alignment> res{l.rows(), r.cols()};
    for (std::size_t i = 0; i < l.rows(); ++i) {
        for (std::size_t j = 0; j < r.cols(); ++j) {
            for (std::size_t k = 0; k < l.cols(); ++k) {
                res(i, j) += l(i, k) * r(k, j);
            }
        }
    }
    return res;
}

template <typename T, std::size_t Alignment>
Matrix<T, Alignment> multiplyReordered(const Matrix<T, Alignment>& l, const Matrix<T, Alignment>& r) {
    assert(l.cols() == r.rows());
    Matrix<T, Alignment> res{l.rows(), r.cols()};
    for (std::size_t i = 0; i < l.rows(); ++i) {
        for (std::size_t k = 0; k < l.cols(); ++k) {
            for (std::size_t j = 0; j < r.cols(); ++j) {
                res(i, j) += l(i, k) * r(k, j);
            }
        }
    }
    return res;
}

template <typename T, std::size_t Alignment>
Matrix<T, Alignment> multiplyBasicThreaded(const Matrix<T, Alignment>& l, const Matrix<T, Alignment>& r,
                                           Threadpool& threadpool) {
    assert(l.cols() == r.rows());
    Matrix<T, Alignment> res{l.rows(), r.cols()};

    auto func = [](Matrix<T, Alignment>& m, const Matrix<T, Alignment>& m1, const Matrix<T, Alignment>& m2,
                   std::size_t i1, std::size_t i2) {
        for (std::size_t i = i1; i < i2; ++i) {
            for (std::size_t j = 0; j < m2.cols(); ++j) {
                for (std::size_t k = 0; k < m1.cols(); ++k) {
                    m(i, j) += m1(i, k) * m2(k, j);
                }
            }
        }
    };

    if (l.rows() < threadpool.size()) {
        std::vector<std::future<void>> futures(l.rows());
        for (std::size_t i = 0; i < l.rows(); ++i) {
            futures[i] = threadpool.createTask(+func, std::ref(res), std::cref(l), std::cref(r), i, i + 1);
        }
        for (const auto& f : futures) {
            f.wait();
        }
        return res;
    }

    const std::size_t step = l.rows() / threadpool.size();
    std::vector<std::future<void>> futures(threadpool.size());
    for (std::size_t i = 0; i < threadpool.size(); ++i) {
        futures[i] = threadpool.createTask(+func, std::ref(res), std::cref(l), std::cref(r), i * step, (i + 1) * step);
    }
    func(res, l, r, l.rows() - l.rows() % threadpool.size(), l.rows());
    for (const auto& f : futures) {
        f.wait();
    }

    return res;
}

template <typename T, std::size_t Alignment>
Matrix<T, Alignment> multiplyReorderedThreaded(const Matrix<T, Alignment>& l, const Matrix<T, Alignment>& r,
                                               Threadpool& threadpool) {
    assert(l.cols() == r.rows());
    Matrix<T, Alignment> res{l.rows(), r.cols()};

    auto func = [](Matrix<T, Alignment>& m, const Matrix<T, Alignment>& m1, const Matrix<T, Alignment>& m2,
                   std::size_t i1, std::size_t i2) {
        for (std::size_t i = i1; i < i2; ++i) {
            for (std::size_t k = 0; k < m1.cols(); ++k) {
                for (std::size_t j = 0; j < m2.cols(); ++j) {
                    m(i, j) += m1(i, k) * m2(k, j);
                }
            }
        }
    };

    if (l.rows() < threadpool.size()) {
        std::vector<std::future<void>> futures(l.rows());
        for (std::size_t i = 0; i < l.rows(); ++i) {
            futures[i] = threadpool.createTask(+func, std::ref(res), std::cref(l), std::cref(r), i, i + 1);
        }
        for (const auto& f : futures) {
            f.wait();
        }
        return res;
    }

    const std::size_t step = l.rows() / threadpool.size();
    std::vector<std::future<void>> futures(threadpool.size());
    for (std::size_t i = 0; i < threadpool.size(); ++i) {
        futures[i] = threadpool.createTask(+func, std::ref(res), std::cref(l), std::cref(r), i * step, (i + 1) * step);
    }
    func(res, l, r, l.rows() - l.rows() % threadpool.size(), l.rows());
    for (const auto& f : futures) {
        f.wait();
    }

    return res;
}

Matrix<float, vf32align> multiplySIMD(const Matrix<float, vf32align>& l, const Matrix<float, vf32align>& r) {
    assert(l.cols() == r.rows());
    Matrix<float, vf32align> res{l.rows(), r.cols()};
    constexpr std::size_t step = vf32::size();
    for (std::size_t i = 0; i < l.rows(); ++i) {
        for (std::size_t k = 0; k < l.cols(); ++k) {
            const vf32 v1{l(i, k)};
            for (std::size_t j = 0; j < r.cols() / step; ++j) {
                const vf32 v{res.data(i, j * step), std::experimental::vector_aligned};
                const vf32 v2{r.data(k, j * step), std::experimental::vector_aligned};
                (v + v1 * v2).copy_to(res.data(i, j * step), std::experimental::vector_aligned);
            }
            for (std::size_t j = l.cols() - l.cols() % vf32::size(); j < l.cols(); ++j) {
                res(i, j) += l(i, k) * r(k, j);
            }
        }
    }
    return res;
}

}  // namespace gpu
