#pragma once

#include <complex>
#include <numbers>

#include <gltf/utils/texture.h>

#include "fresnel.hpp"
#include "ggx.hpp"
#include "gltf/gvec.h"
#include "gltf/types.h"
#include "gltf/utils/orthonormal.h"
#include "sample.hpp"
#include "scene.hpp"

namespace gpu::random {

class BSDFSampler {
    Scene const &scene;
    std::vector<std::optional<gltf::Material>> material;  // geometryI -> Material
    std::vector<gltf::utils::Image<f32>> image;
    std::vector<gltf::utils::Texture<f32>> texture;

public:
    BSDFSampler(Scene const &sceneCRef) noexcept : scene(sceneCRef) {
        auto const materialR = scene.tlas.blasInfo | std::views::transform([this](auto const &info) noexcept {
                                   return info.materialI.transform(lambdaR1(i, scene.gltf.json.materials[i]));
                               });
        material             = std::vector<std::optional<gltf::Material>>{
            std::ranges::begin(materialR),
            std::ranges::end(materialR),
        };

        auto const imageR = scene.gltf.json.images |
                            std::views::transform([this](gltf::Image const &img) noexcept -> gltf::utils::Image<f32> {
                                return {scene.gltf, img};
                            });
        image = std::vector<gltf::utils::Image<f32>>{std::ranges::begin(imageR), std::ranges::end(imageR)};

        auto const textureR = std::views::iota(0u, u32(scene.gltf.json.textures.size())) |
                              std::views::transform([this](u32 const i) noexcept -> gltf::utils::Texture<f32> {
                                  gltf::Texture const &tex = scene.gltf.json.textures[i];
                                  return {scene.gltf.json.samplers[tex.sampler.value_or(0u)], image[tex.source]};
                              });
        texture = std::vector<gltf::utils::Texture<f32>>{std::ranges::begin(textureR), std::ranges::end(textureR)};
    }

    vec3 albedo(Scene::SurfacePoint const &q) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q.intersection;
        auto const pbr                                    = material[geometryI]->pbrMetallicRoughness;
        vec4 const baseColor =
            pbr->baseColorTexture.transform(lambdaR1(x, texture[x.index].sample(q.tex))).value_or(vec4(1.f));
        return baseColor * pbr->baseColorFactor;
    }

    std::optional<f32> IOR(Scene::SurfacePoint const &q) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q.intersection;
        return material[geometryI]
            ->extensions.and_then(lambdaE1(ext, ext.KHR_materials_ior))
            .transform(lambdaE1(ext, ext.ior));
    }

    vec2 metallicRoughness(Scene::SurfacePoint const &q) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q.intersection;
        auto const pbr                                    = material[geometryI]->pbrMetallicRoughness;
        vec4 const mrt = pbr->metallicRoughnessTexture.transform(lambdaR1(x, texture[x.index].sample(q.tex)))
                             .value_or(vec4(0.f, 1.f, 0.f, 0.f));
        return {
            std::clamp(pbr->metallicFactor * mrt.z, 0.f, 1.f),
            IOR(q) ? 1e-2f : std::clamp(pbr->roughnessFactor * mrt.y, 1e-2f, 1.f),
        };
    }

    GGXDistribution ggxOn(Scene::SurfacePoint const &q) const noexcept {
        f32 const roughness = metallicRoughness(q).y;
        return {
            .a   = vec2(roughness * roughness),
            .TBN = gltf::utils::orthonormal(q.norm),
        };
    }

    gltf::gvec<vec3, 2> fresnel(vec3 const wi, Scene::SurfacePoint const &q) const noexcept {
        f32 const metallic  = metallicRoughness(q).x;
        auto const maybeIOR = IOR(q);

        if (maybeIOR) {
            vec3 const R = fresnelReflectance(wi, q.norm, c32(*maybeIOR, 0.f));
            return {R, albedo(q) * (1.f - metallic) * (vec3(1.f) - R)};
        }
        if (metallic == 1.f) {
            vec3 const R = fresnelReflectanceF0(wi, q.norm, albedo(q));
            return {R, vec3(0.f)};
        } else {
            return {vec3(0.f), albedo(q)};  // {R, albedo(q) * (vec3(1.f) - R)};
        }
    }

    gltf::gvec<vec3, 2> fresnel(vec3 const wi, vec3 const norm, Scene::SurfacePoint const &q) const noexcept {
        f32 const metallic  = metallicRoughness(q).x;
        auto const maybeIOR = IOR(q);

        if (maybeIOR) {
            vec3 const R = fresnelReflectance(wi, norm, c32(*maybeIOR, 0.f));
            return {R, albedo(q) * (1.f - metallic) * (vec3(1.f) - R)};
        }
        if (metallic == 1.f) {
            vec3 const R = fresnelReflectanceF0(wi, norm, albedo(q));
            return {R, vec3(0.f)};
        } else {
            return {vec3(0.f), albedo(q)};  // {R, albedo(q) * (vec3(1.f) - R)};
        }
    }

    // light flow direction: q0 -> q1 -> q2
    vec3 BSDF(Scene::SurfacePoint const &q0, Scene::SurfacePoint const &q1,
              Scene::SurfacePoint const &q2) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q1.intersection;
        auto const pbr                                    = material[geometryI]->pbrMetallicRoughness;
        vec4 const baseColor =
            pbr->baseColorTexture.transform(lambdaR1(x, texture[x.index].sample(q1.tex))).value_or(vec4(1.f));
        vec3 const albedo = baseColor * pbr->baseColorFactor;
        if (pbr->roughnessFactor == 1.f) return albedo / std::numbers::pi_v<f32>;

        std::optional<f32> const maybeIOR = material[geometryI]
                                                ->extensions.and_then(lambdaE1(ext, ext.KHR_materials_ior))
                                                .transform(lambdaE1(ext, ext.ior));
        f32 const n = maybeIOR.value_or(1.f);

        vec3 const wi = normalize(q0.pos - q1.pos);
        vec3 const wo = normalize(q2.pos - q1.pos);
        vec3 const wh = reflectHalfway(wi, q1.norm, wo);
        vec3 const wt = refractHalfway(wi, q1.norm, wo, n);

        auto const [reflR, reflT] = fresnel(wi, wh, q1);
        auto const [refrR, refrT] = fresnel(wi, wt, q1);

        GGXDistribution const ggx = ggxOn(q1);

        vec3 const refl =
            reflR * ggx.D(wh) * ggx.G2Refl(wi, wh, wo) * reflectJacobian(wi, q1.norm) / gltf::abs(dot(wo, q1.norm));
        vec3 const refr = refrT * ggx.D(wt) * ggx.G2Refr(wi, wt, wo) * refractJacobian(wi, q1.norm, wo, n) *
                          gltf::abs(dot(wi, wt) / (dot(wi, q1.norm) * dot(wo, q1.norm)));
        return maybeIOR ? refl + refr : refl + refrT * std::numbers::inv_pi_v<f32>;
    }

    std::optional<Scene::SurfacePointSample> sample(Scene::SurfacePoint const &q1,
                                                    Scene::SurfacePoint const &q2) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q1.intersection;
        auto const pbr                                    = material[geometryI]->pbrMetallicRoughness;
        if (pbr->roughnessFactor == 1.f) {
            vec3 const wi = uniformHemisphereSample(q1.norm);
            return scene.closestHit({q1.pos, wi}, {1e-4f / dot(q1.norm, wi), 1.f / 0.f})
                .transform([&](Scene::SurfacePoint const &q0) noexcept -> Scene::SurfacePointSample {
                    return {q0, std::numbers::inv_pi_v<f32>};
                });
        }

        std::optional<f32> const maybeIOR = material[geometryI]
                                                ->extensions.and_then(lambdaE1(ext, ext.KHR_materials_ior))
                                                .transform(lambdaE1(ext, ext.ior));

        vec3 const wo     = normalize(q2.pos - q1.pos);
        auto const [R, T] = fresnel(wo, q1);

        GGXDistribution const ggx = ggxOn(q1);
        GGXVisibleMicrofacetSampler const vggx{ggx};

        if (!maybeIOR) {
            GGXVisibleReflectionSampler const refl{vggx};
            vec3 const wi = refl.sample(wo);
            if (dot(wi, q1.norm) * dot(wo, q1.norm) <= 0.f) {
                return std::nullopt;
            }
            f32 const pdf = refl.pdfW(wo, wi);
            if (pdf <= 0.f) {
                return std::nullopt;
            }
            return scene.closestHit({q1.pos, wi}, {1e-4f / gltf::abs(dot(wi, q1.norm)), 1.f / 0.f})
                .transform([&](Scene::SurfacePoint const &q0) noexcept -> Scene::SurfacePointSample {
                    return {q0, pdf};
                });
        }

        f32 const x     = generateUniformFloat();
        vec3 const luma = vec3(0.2126f, 0.7152f, 0.0722f);
        f32 const X     = std::min(1.f, dot(luma, R) / dot(luma, R + T));
        if (x < X) {
            GGXVisibleReflectionSampler const refl{vggx};
            vec3 const wi = refl.sample(wo);
            if (dot(wi, q1.norm) * dot(wo, q1.norm) <= 0.f) {
                return std::nullopt;
            }
            f32 const pdf = X * refl.pdfW(wo, wi);
            if (pdf <= 0.f) {
                return std::nullopt;
            }
            return scene.closestHit({q1.pos, wi}, {1e-4f / gltf::abs(dot(wi, q1.norm)), 1.f / 0.f})
                .transform([&](Scene::SurfacePoint const &q0) noexcept -> Scene::SurfacePointSample {
                    return {q0, pdf};
                });
        } else {
            f32 const n = std::max(1.1f, maybeIOR.value_or(1.0f));
            GGXVisibleRefractionSampler refr{n, vggx};
            vec3 const wi = refr.sample(wo);
            if (dot(wi, q1.norm) * dot(wo, q1.norm) >= 0.f) {
                return std::nullopt;
            }
            f32 pdf = (1.f - X) * refr.pdfW(wo, wi);
            if (pdf <= 0.f) {
                return std::nullopt;
            }
            return scene.closestHit({q1.pos, wi}, {1e-4f / gltf::abs(dot(wi, q1.norm)), 1.f / 0.f})
                .transform([&](Scene::SurfacePoint const &q0) noexcept -> Scene::SurfacePointSample {
                    return {q0, pdf};
                });
        }
    }

    f32 pdfW(Scene::SurfacePoint const &q0, Scene::SurfacePoint const &q1,
             Scene::SurfacePoint const &q2) const noexcept {
        auto const [instanceI, geometryI, triangleI, rti] = q1.intersection;
        auto const pbr                                    = material[geometryI]->pbrMetallicRoughness;
        if (pbr->roughnessFactor == 1.f) {
            return std::numbers::inv_pi_v<f32>;
        }

        vec3 const wi = normalize(q0.pos - q1.pos);
        vec3 const wo = normalize(q2.pos - q1.pos);

        auto const [R, T] = fresnel(wo, q1);
        vec3 const luma   = vec3(0.2126f, 0.7152f, 0.0722f);
        f32 const X       = std::min(1.f, dot(luma, R) / dot(luma, R + T));

        std::optional<f32> const maybeIOR = material[geometryI]
                                                ->extensions.and_then(lambdaE1(ext, ext.KHR_materials_ior))
                                                .transform(lambdaE1(ext, ext.ior));
        f32 const n = std::max(1.1f, maybeIOR.value_or(1.0f));

        GGXVisibleMicrofacetSampler const vggx{.ggx = ggxOn(q1)};

        GGXVisibleReflectionSampler const refl{vggx};
        GGXVisibleRefractionSampler const refr{n, vggx};

        return X * refl.pdfW(wo, wi) + (1.f - X) * (maybeIOR ? refr.pdfW(wo, wi) : std::numbers::inv_pi_v<f32>);
    }
};

}  // namespace gpu::random

