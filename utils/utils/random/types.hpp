#pragma once

#include <complex>

#include <gltf/types.h>

namespace gpu::random {

using gltf::i16;
using gltf::i32;
using gltf::i64;
using gltf::i8;

using gltf::u16;
using gltf::u32;
using gltf::u64;
using gltf::u8;

using gltf::f32;
using gltf::f64;

using c32 = std::complex<f32>;

using gltf::vec2;
using gltf::vec3;
using gltf::vec4;

using gltf::ivec2;
using gltf::ivec3;
using gltf::ivec4;

using gltf::uvec2;
using gltf::uvec3;
using gltf::uvec4;

using gltf::mat2;
using gltf::mat3;
using gltf::mat4;
using gltf::mat4x3;

}  // namespace gpu::random
