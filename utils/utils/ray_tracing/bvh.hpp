#pragma once

#include <algorithm>
#include <memory>
#include <ranges>
#include <utility>
#include <variant>
#include <vector>

#include "aabb.hpp"
#include "ray.hpp"

namespace gpu {

template <typename Float, typename T>
struct BVHNode {
    struct Children {
        std::unique_ptr<BVHNode<Float, T>> left;
        std::unique_ptr<BVHNode<Float, T>> right;
    };

    AABB<Float> box;
    std::variant<Children, T> data;
};

template <typename Float, typename T>
using BVH = std::unique_ptr<BVHNode<Float, T>>;

namespace details {

template <typename Float>
using IBox = std::pair<unsigned, AABB<Float>>;

template <typename Float, std::ranges::random_access_range Boxable>
auto createVBHImpl(std::span<IBox<Float>> ibox, Boxable &&r) -> BVH<Float, std::ranges::range_value_t<Boxable>> {
    using T = std::ranges::range_value_t<Boxable>;

    if (ibox.size() == 1) {
        return std::make_unique<BVHNode<Float, T>>(ibox.front().second, std::move(r[ibox.front().first]));
    }

    const AABB commonBox = *std::ranges::fold_left_first(ibox | std::views::values, std::plus{});
    const vec3 diag      = commonBox.rMax - commonBox.rMin;

    const auto proj = [i = diag.x > diag.y ? (diag.x > diag.z ? 0u : 2u)
                                           : (diag.y > diag.z ? 1u : 2u)](const IBox<Float> &box) {
        const auto &[rMin, rMax] = box.second;
        return rMin[i] + rMax[i];
    };

    const unsigned midPoint = static_cast<unsigned>(ibox.size() + 1u) / 2u;
    std::ranges::nth_element(ibox, ibox.begin() + midPoint, std::ranges::less{}, proj);

    return std::make_unique<BVHNode<Float, T>>(
        commonBox, typename BVHNode<Float, T>::Children{.left  = createVBHImpl(ibox.first(midPoint), r),
                                                        .right = createVBHImpl(ibox.subspan(midPoint), r)});
}

}  // namespace details

template <typename Float, std::ranges::random_access_range Boxable, typename F>
[[nodiscard]] auto createBVH(Boxable &&r, const F &toBox) {
    const auto boxER =
        r | std::views::enumerate | std::views::transform([&toBox](const auto &pair) -> details::IBox<Float> {
            const auto &[i, obj] = pair;
            return {static_cast<unsigned>(i), toBox(obj)};
        });

    std::vector<details::IBox<Float>> ibox(std::ranges::begin(boxER), std::ranges::end(boxER));
    return details::createVBHImpl<Float>(ibox, std::forward<Boxable>(r));
}

template <typename T, typename I>
[[nodiscard]] auto hitDistance(const std::pair<const T *, I> &intersection) {
    return hitDistance(intersection.second);
}

template <typename Float, typename T>
[[nodiscard]] auto intersection(const Ray<Float> &ray, const RayRange<Float> &range, const BVH<Float, T> &bvh)
    -> std::pair<const T *, decltype(intersection(ray, range, std::declval<T>()))> {
    if (std::holds_alternative<T>(bvh->data)) {
        const T &geometry = std::get<T>(bvh->data);
        return {&geometry, intersection(ray, range, geometry)};
    }

    const auto &[left, right]    = std::get<typename BVHNode<Float, T>::Children>(bvh->data);
    const auto leftIntersection  = intersection(ray, range, left->box);
    const auto rightIntersection = intersection(ray, range, right->box);

    if (!hit(leftIntersection) && !hit(rightIntersection)) {
        return {nullptr, {}};
    }
    if (!hit(leftIntersection)) {
        return intersection(ray, range, right);
    }
    if (!hit(rightIntersection)) {
        return intersection(ray, range, left);
    }

    const Float leftDistance  = hitDistance(leftIntersection);
    const Float rightDistance = hitDistance(rightIntersection);

    const bool less      = leftDistance < rightDistance;
    const auto &closest  = less ? left : right;
    const auto &furthest = less ? right : left;

    const auto closestIntersection = intersection(ray, range, closest);
    if (!closestIntersection.first || !hit(closestIntersection.second)) {
        return intersection(ray, range, furthest);
    }
    const Float closestDistance = hitDistance(closestIntersection);

    const auto furthestIntersection = intersection(ray, {range.tMin, closestDistance}, furthest);
    if (!furthestIntersection.first || !hit(furthestIntersection.second)) {
        return closestIntersection;
    }
    const Float furthestDistance = hitDistance(furthestIntersection);

    return closestDistance < furthestDistance ? closestIntersection : furthestIntersection;
}

}  // namespace gpu
