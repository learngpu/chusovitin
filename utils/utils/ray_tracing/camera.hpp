#pragma once

#include "ray.hpp"
#include "vec3.hpp"

namespace gpu {

struct Camera {
    vec3<float> origin;
    vec3<float> at;
    vec3<float> up;
    float fov;
    float aspectRatio;

    const vec3<float> z = normalize(origin - at);
    const vec3<float> x = normalize(cross(up, z));
    const vec3<float> y = cross(z, x);
    const float tan     = std::tan(fov);

    [[nodiscard]] Ray<float> castRay(float u, float v) const {
        const vec3 d = x * (u * tan * aspectRatio) + y * (v * tan) - z;
        return {.origin = origin, .direction = normalize(d)};
    }

    [[nodiscard]] Ray<f32x4> castRay(const f32x4 &u, const f32x4 &v) const {
        const vec3x4 xv{x.x, x.y, x.z};
        const vec3x4 yv{y.x, y.y, y.z};
        const vec3x4 zv{z.x, z.y, z.z};
        const vec3 d = xv * (u * tan * aspectRatio) + yv * (v * tan) - zv;
        return Ray<f32x4>{.origin = {origin.x, origin.y, origin.z}, .direction = normalize(d)};
    }
};

}  // namespace gpu
