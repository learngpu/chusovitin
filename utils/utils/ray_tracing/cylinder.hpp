#pragma once

#include <cmath>

#include "ray.hpp"

namespace gpu {

template <typename Float>
struct Cylinder {
    vec3<Float> r1;
    vec3<Float> r2;
    Float radius;
};

template <typename Float>
[[nodiscard]] RayRange<Float> intersection(const Ray<Float> &ray, const RayRange<Float> &range,
                                           const Cylinder<Float> &cylinder) {
    Float tMin[2] = {};
    Float tMax[2] = {};
    // intersection with side surface
    const vec3 v   = cylinder.r2 - cylinder.r1;
    const Float v2 = squaredNorm(v);

    const Float dv  = dot(ray.direction, v);
    const Float dv2 = squaredNorm(ray.direction) * v2 - dv * dv;

    const vec3 o    = ray.origin - cylinder.r1;
    const Float ov  = dot(o, v);
    const Float ov2 = squaredNorm(o) * v2 - ov * ov;

    const Float ovdv = dot(o, ray.direction) * v2 - ov * dv;

    const Float det     = ovdv * ovdv - dv2 * (ov2 - cylinder.radius * cylinder.radius * squaredNorm(v));
    const Float detSqrt = sqrt(det);
    tMin[0]             = (-ovdv - detSqrt) / dv2;
    tMax[0]             = (-ovdv + detSqrt) / dv2;
    // intersection with base planes
    const Float t1 = -dot(ray.origin - cylinder.r1, v) / dot(ray.direction, v);
    const Float t2 = -dot(ray.origin - cylinder.r2, v) / dot(ray.direction, v);

    tMin[1] = minf(t1, t2);
    tMax[1] = maxf(t1, t2);
    if constexpr (std::is_same_v<Float, float>) {
        if (det < 0.f) {
            return {.tMin = detSqrt, .tMax = detSqrt};
        }
        const RayRange r{.tMin = maxf(tMin[0], maxf(tMin[1], range.tMin)),
                         .tMax = minf(tMax[0], minf(tMax[1], range.tMax))};
        return r;
    } else {
        const auto m = det < 0.f;
        RayRange r{.tMin = maxf(tMin[0], maxf(tMin[1], range.tMin)), .tMax = minf(tMax[0], minf(tMax[1], range.tMax))};
        where(m, r.tMin) = detSqrt;
        where(m, r.tMax) = detSqrt;
        return r;
    }
}

template <typename Float>
inline vec3<Float> normal(const vec3<Float> &pos, const Cylinder<Float> &cylinder) {
    const vec3 d = pos - cylinder.r1;
    const vec3 v = cylinder.r2 - cylinder.r1;

    const Float d2 = squaredNorm(d);
    const Float r2 = cylinder.radius * cylinder.radius;

    if constexpr (std::is_same_v<float, Float>) {
        if (d2 < r2) {
            return -normalize(v);
        }
        if (squaredNorm(pos - cylinder.r2) < r2) {
            return normalize(v);
        }
        const Float v2 = squaredNorm(v);
        const Float dv = dot(d, v);

        return normalize(d - (dv / v2) * v);
    } else {
        const Float v2 = squaredNorm(v);
        const Float dv = dot(d, v);
        vec3 res       = normalize(d - (dv / v2) * v);
        const vec3 n   = normalize(v);

        const auto m1    = d2 < r2;
        where(m1, res.x) = -n.x;
        where(m1, res.y) = -n.y;
        where(m1, res.z) = -n.z;

        const auto m2    = squaredNorm(pos - cylinder.r2) < r2;
        where(m2, res.x) = n.x;
        where(m2, res.y) = n.y;
        where(m2, res.z) = n.z;

        return res;
    }
}

}  // namespace gpu
